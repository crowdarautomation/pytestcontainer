# Production requirements
-r prod.txt

# Testing
coverage==4.5.4
pytest==5.0.1
pytest-cov==2.7.1
requests-mock==1.6.0 
