FROM python:3.7.7-buster

RUN mkdir /files

WORKDIR /files

COPY . .

RUN pip install --upgrade pip
RUN pip install -r requirements/dev.txt

